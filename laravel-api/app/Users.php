<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable = ['username','email','name', 'id'];
    protected $primaryKey = 'id';
    protected $foreignKey = 'roles_id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected static function boot()
    {
        parent:boot();
        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
